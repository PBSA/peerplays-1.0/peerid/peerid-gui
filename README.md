# PeerID - GUI

This is the front-end app through which site administrators will manage their PeerID installations.  You will need to install and configure the [PeerID backend](https://gitlab.com/PBSA/peerid/peerid-backend) before beginning with the PeerID - GUI install. 

## Configuring .env file for connection to the backend

An example .env file is available inside the repository, you will first need to copy this:

`cp .env.example .env`

Now you will need to edit the .env to connect to either your local testnet, or the running testnet

The application requires certain parameters to be provided such as the API endpoint for calls to the backend node server API:

```
# Location of PeerID backend API, either localhost or specified URL in your nginx config
DEV_API_ROUTE='http://localhost:3000/'
PRODUCTION_API_ROUTE='http://localhost:3000/'

DEV_BASE_ROUTE='http://localhost:8082'
PRODUCTION_BASE_ROUTE='http://localhost:8082'


#Testnet witness_node api location, this can be localhost if your peerid deployments on the same system as your testnet
BLOCKCHAIN_ENDPOINTS='wss://mint.peerplays.download/api'

# Test Asset ID
PEERPLAYS_USD_ASSET_ID='1.3.0'

# These will depend on what method you followed here - https://gitlab.com/PBSA/peerid/peerid-backend/-/tree/develop/docs
PEERPLAYS_ESCROW_ACCOUNT_ID='1.2.x'
PEERPLAYS_PAYMENT_ACCOUNT_ID='1.2.x'

```

The API routes mentioned above are the urls for the PeerID backend. Start PeerID backend before starting this project.

## Installing and Starting the app

Once the `.env` file has been copied,  use the command - `npm i` to install the dependencies

Then the command - `npm run start` to start the app.

The app will build and run at `http://localhost:8082`

## Recommendations

It is recommended to run this application using pm2 - This can be installed with the following:

`npm install pm2 -g` 

To run the gui with pm2, you can use the following command:

`pm2 --name peerid-gui start npm -- start` 

Its also recommended to use a reverse proxy for serving this application - An example can be found below:

```
server {
  server_name <your URL/DOMAIN>;
  root /var/www/peerid-gui;

   add_header 'Access-Control-Allow-Origin' '*' always;
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
   add_header 'Access-Control-Allow-Headers' 'X-Requested-With,Accept,Content-Type,Authorization, Origin' always;


  location / {
   add_header 'Access-Control-Allow-Origin' '*' always;
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
   add_header 'Access-Control-Allow-Headers' 'X-Requested-With,Accept,Content-Type,Authorization, Origin' always;

  proxy_pass http://localhost:8082;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
  }

}
```
